import * as React from "react";
import {
  Divider,
  ApplicationProvider,
  Input,
  Button,
} from "@ui-kitten/components";
import * as eva from "@eva-design/eva";

import {
  Text,
  FlatList,
  View,
  StyleSheet,
  Image,
  Modal,
  TouchableHighlight,
  SafeAreaView,
} from "react-native";
import { Header, SearchBar, Badge, Icon } from "react-native-elements";
import { AppLoading } from "expo";

export default function CreateScreen() {
  /**let [fontsLoaded] = useFonts({
    OpenSans: require("../assets/OpenSans-Regular.ttf"),
    OpenSansBold: require("../assets/OpenSans-Bold.ttf"),
  });*/

  const [quizName, setQuizName] = React.useState("");
  const [quizQuestion, setQuizQuestion] = React.useState("");
  const [option1, setOption1] = React.useState("");
  const [option2, setOption2] = React.useState("");
  const [visibleModal1, setVisibleModal1] = React.useState(false);
  const [visibleModal2, setVisibleModal2] = React.useState(false);

  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <View style={styles.container1}>
        <Text style={styles.quizName}>Quiz Name</Text>
        <Input
          placeholder="Enter quiz name"
          value={quizName}
          style={styles.quizNameInput}
          onChangeText={(value) => setQuizName(value)}
        />
      </View>

      <View style={styles.container2}>
        <View style={styles.sameLineContainer}>
          <Text style={styles.question1}>Question 1 of 1</Text>
          <Icon
            name="add"
            color="#007AFF"
            onPress={() => setVisibleModal1(true)}
          />
        </View>
      </View>

      <View style={styles.container3}>
        <Text style={styles.quizQuestion}>Enter Question</Text>
        <Input
          multiline={true}
          value={quizQuestion}
          style={styles.questionInput}
          onChangeText={(changedValue) => setQuizQuestion(changedValue)}
          placeholder="Enter Question"
        />
      </View>

      <View style={styles.container4}>
        <View style={styles.sameLineContainer}>
          <Text style={styles.option}>Add Options</Text>
          <Icon
            name="add"
            color="#007AFF"
            onPress={() => setVisibleModal2(true)}
          />
        </View>
      </View>

      <View style={styles.container5}>
        <Text style={styles.option1}>Option 1</Text>
        <Input
          placeholder="Enter option text"
          value={option1}
          style={styles.option1Input}
          onChangeText={(value) => setOption1(value)}
        />
      </View>

      <View style={styles.container6}>
        <Text style={styles.option2}>Option 2</Text>
        <Input
          placeholder="Enter option text"
          value={option2}
          style={styles.option2Input}
          onChangeText={(value) => setOption2(value)}
        />
      </View>

      <View style={styles.container7}>
        <Button style={styles.saveButton}>
          <Text style={styles.saveText}>Save Quiz</Text>
        </Button>
      </View>
    </ApplicationProvider>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#FFFFFF",
  },
  sameLineContainer: { flexDirection: "row", justifyContent: "space-between" },
  container1: {
    marginTop: 16,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 9,
  },
  container2: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 14,
  },
  container3: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 18,
  },
  container4: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  container5: { marginLeft: 20, marginRight: 20, marginBottom: 14 },
  container6: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 202,
  },
  container7: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
  },
  quizName: {
    width: "100%",
    height: 16,
    //left: 20,
    //top: 120,
    //fontFamily: "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.096,
    color: "#000000",
    marginBottom: 9,
  },
  quizNameInput: {
    left: "0%",
    right: "0%",
    top: "0%",
    bottom: "0%",
    //borderWidth: 1,
    borderColor: "#C8C8CC",
    // borderStyle: "solid",
    //boxSizing: "border-box",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  question1: {
    width: "100%",
    height: 19,
    //"left": 20,
    //"top": 198,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "600",
    fontSize: 14,
    lineHeight: 19,
    letterSpacing: -0.24,
    color: "#000000",
  },
  quizQuestion: {
    width: "100%",
    height: 16,
    //"left": 20,
    //"top": 231,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.096,
    color: "#000000",
    marginBottom: 9,
  },
  questionInput: {
    left: "0%",
    right: "0%",
    top: "0%",
    bottom: "0%",
    //borderWidth: 1,
    borderColor: "#C8C8CC",
    //borderStyle: "solid",
    //boxSizing: "border-box",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  option: {
    width: "100%",
    height: 16,
    //"left": 20,
    //"top": 362,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.096,
    color: "#000000",
  },
  option1: {
    width: "100%",
    height: 16,
    //"left": 20,
    //"top": 398,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.096,
    color: "#000000",
    marginBottom: 9,
  },
  option1Input: {
    left: "0%",
    right: "0%",
    top: "0%",
    bottom: "0%",
    //"borderWidth": 1,
    borderColor: "#C8C8CC",
    //"borderStyle": "solid",
    //"boxSizing": "border-box",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  option2: {
    width: "100%",
    height: 16,
    //"left": 20,
    //"top": 398,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.096,
    color: "#000000",
    marginBottom: 9,
  },
  option2Input: {
    left: "0%",
    right: "0%",
    top: "0%",
    bottom: "0%",
    //"borderWidth": 1,
    borderColor: "#C8C8CC",
    //"borderStyle": "solid",
    //"boxSizing": "border-box",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
  saveText: {
    width: "100%",
    height: 21,
    //"left": 123,
    //"top": 761,
    //"fontFamily": "Open Sans",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
    letterSpacing: 0.261333,
    color: "#FFFFFF",
  },
  saveButton: {
    width: "100%",
    height: 40,
    //left: 20,
    //top: 752,
    backgroundColor: "#5655C6",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomLeftRadius: 6,
  },
});
