import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  Image,
  TouchableOpacity,
  AppRegistry,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CreateScreen from "./screens/CreateScreen.js";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="CreateScreen"
          component={CreateScreen}
          options={({ navigation, route }) => ({
            headerTitle: "Create Quiz",
            headerStyle: {
              backgroundColor: "#5655C6",
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowRadius: 0,
              shadowColor: "#F3F2F1",
              shadowOpacity: 1,
              //backdropFilter: "blur(54.3656px)",
              // height: 145,
            },
            headerLeft: () => (
              <Text
                style={{
                  width: "100%",
                  height: 28,
                  left: 26,
                  //top: 62,
                  //  fontFamily: "OpenSans",
                  fontStyle: "normal",
                  fontWeight: "bold",
                  fontSize: 21,
                  lineHeight: 28,
                  color: "#FFFFFF",
                }}
              >
                Groups
              </Text>
            ),
            headerRight: () => (
              <TouchableOpacity
                onPress={() => navigation.navigate("ApprovalsScreen")}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    width: "100%",
                    height: 19,
                    right: 5,
                    fontStyle: "normal",
                    fontWeight: "normal",
                    fontSize: 16,
                    lineHeight: 22,
                    textAlign: "center",
                    //  fontFamily: "OpenSans",
                    letterSpacing: 0.261333,
                  }}
                >
                  Save
                </Text>
              </TouchableOpacity>
            ),
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 30.33,
  },
});
